# amefAuto
 Use to build and upload lastest firmware to device automatically

NOTE!!! 
  You must to change these paths in file amefAuto.py to your own before installing:
  IMAGE_DIR ="/home/ndhau/accediancode/Develop_branch/amef.image"
  AMEF_DIR = "/home/ndhau/accediancode/Develop_branch/amef"

1. Install: 

    sudo ./install.sh 

2. Usage: 
amefbuild: automatic build and upload firmware to device
optional arguments:
  -h, --help            show this help message and exit
  -i IP                 Input device IP
  -p {ETCHELL4,ETCHELL5,CLIPPER,VCX,FSX}
                        Input device platform
  -b                    Build firmware only, do not upload
  -u                    Upload lastest firmware to device
  -bu                   Build and Upload lastest firmware to device
