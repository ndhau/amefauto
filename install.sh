#!/bin/sh

BIN=amefAuto
BIN_PATH=/usr/local/bin/
DIRNAME=`pwd`
SOURCE_FILE=$DIRNAME/amefAuto.py

echo "path file: $SOURCE_FILE"

ln -fs $SOURCE_FILE $BIN_PATH/$BIN

if [ $? -ne 0 ]
then
    echo "Install ERROR!\n"
else
    echo "\nInstall 'amefAuto' successfully. Type 'amefAuto -h' to use\n"
    exit 0
fi


