#!/usr/bin/env python
################################################################
#  Author: Hau Nguyen
#  Usage: 
#       Use to build and upload lastest firmware to device automatically
#  Syntax: 
#       amefAuto -i <Device_IP> -p <plaform> -<option> 
# 
# 
#############################################################
import pexpect, signal, sys, os
import argparse
import random
import sys
import socket

##### NOTE !!: Must to change these path to your owns ###############################

IMAGE_DIR ="/home/ndhau/accediancode/Develop_branch/amef.image"
AMEF_DIR = "/home/ndhau/accediancode/Develop_branch/amef"
#####################################################################################


BUILDA_PATH = AMEF_DIR + "/scripts/Builda"
HTTP_PORT = random.randint(10000,60000)
str_success = "**************************************************************************"
ls_expect = []


def getHostIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    host_IP = s.getsockname()[0]
    s.close()
    return host_IP

def SIGINT_handler(signal, frame):
    closeAllExpect()
    print "\nRecevied an interruped signal. Bye!"
    sys.exit(0)

def closeAllExpect():
    for p in ls_expect:
        p.sendcontrol('c')

# Author: Nghia Nguyen
class MyConnection:

    SSH_TIMEOUT      = 30
    SSH_NEWKEY       = "Are you sure you want to continue connecting (yes/no)?"
    SSH_REQUEST_PW   = "'s password:"
    DEFAULT_PASSWORD = "admin"
    DEFAULT_USER     = "admin"

    NOMAL_USER_PREFIX   = "\$ $"
    NOMAL_ROOT_PREFIX   = "\# $"
    ACCEDIAN_PREFIX     = ".\d{3}-\d{4}: $"

    def __init__(self, ip=""):
        self.exp        = pexpect
        self.prefix_exp = MyConnection.ACCEDIAN_PREFIX
        self.ip         = ip
        self.user       = ""
        self.password   = ""
        self.arr_prefix = [
            MyConnection.SSH_NEWKEY,
            MyConnection.SSH_REQUEST_PW,
            pexpect.EOF,
            MyConnection.ACCEDIAN_PREFIX,
            MyConnection.NOMAL_USER_PREFIX,
            MyConnection.NOMAL_ROOT_PREFIX
        ]

    def login (self, us=DEFAULT_USER, pw=DEFAULT_PASSWORD, prefix=ACCEDIAN_PREFIX, timeout=SSH_TIMEOUT):
        self.user       = us
        self.password   = pw
        return self.__login_raw(self.ip, self.user, self.password, prefix, timeout)

    def __login_raw (self, ip, u, pw, prefix=ACCEDIAN_PREFIX, time_out=SSH_TIMEOUT):
            print "Login: ssh %s@%s" % (u, ip)
            self.exp = pexpect.spawn("ssh %s@%s" % (u, ip) )

            if (prefix not in self.arr_prefix):
                self.arr_prefix.append(prefix)

            # Loop 1 (yes/no) + 3(if enter wrong pass)
            for i in range(4):
                rs = self.exp.expect(self.arr_prefix, timeout=time_out)
                if rs==self.arr_prefix.index(MyConnection.SSH_NEWKEY):
                    self.exp.sendline("yes")

                elif rs==self.arr_prefix.index(MyConnection.SSH_REQUEST_PW):
                    self.exp.sendline(pw)

                elif rs == self.arr_prefix.index(pexpect.EOF):
                    break

                elif rs>=0:
                    # self.prefix_exp = self.exp.before.split("\n")[-1]
                    self.prefix_exp = self.arr_prefix[rs]
                    return self

            raise Exception('[ERROR] Login failed. IP: {}'.format(ip))

    def logout (self):
        self.exp.sendline("exit")
        self.exp.expect(pexpect.EOF);

    def writelock(self):
        self.exp.sendline("session writelock")
        self.exp.expect(self.prefix_exp);

    def writeunlock(self):
        self.exp.sendline("session writeunlock")
        self.exp.expect(self.prefix_exp);

    # TODO: timeout is not supported
    def send(self, cmd, expected=None, timeout=None, waiting=True):
        print "[%s] %s" % (self.ip, cmd)
        
        self.exp.sendline(cmd)

        if not waiting:
            return 0

        if (expected):
            #print "prefix:", expected
            rs = self.exp.expect(expected)
        else:
            #print "prefix:", self.prefix_exp
            rs = self.exp.expect(self.prefix_exp)
        print self.exp.before
        return rs

    def send_raw(self, cmd):
        print "[%s] %s" % (self.ip, cmd)
        self.exp.sendline(cmd)
    # TODO: timeout
    def expect_raw(self, expected=ACCEDIAN_PREFIX, time_out=None):
        self.exp.expect(expected, timeout=time_out)

class AmefBuild_Auto():
    """docstring for AmefBuild_Auto"""

    def __init__(self, ip, platform=None, config_reset=False):
        self.ip = device_ip
        self.platform= platform
        self.host_ip = getHostIP()
        self.config_reset = config_reset
    
    def build_amef(self):
        print "******* Start to build Firmware {} *********".format(self.platform)

        # TODO: run bashrc when spawn
        # bashrc = os.path.abspath(os.path.join(os.path.expanduser('~'),".bashrc"))
        p_build = pexpect.spawn("{path} -so -p {platform}".format(path=BUILDA_PATH, platform=self.platform))
        
        # redirect logfile
        p_build.logfile = sys.stdout 
        
        # Store pexect sesion into list for manager:
        ls_expect.append(p_build)

        # Waiting to finish building firmware
        p_build.expect(pexpect.EOF, timeout=None)

        if (p_build.before.split()[-1] != str_success):
            sys.exit(0)

    def upload_firmware(self):
        print "******* Uploading firmware {} to device {}  **   *******".format(self.platform , self.ip)
        
        self.start_http_server()

        # Uploading the firmware
        ip = self.ip
        device = MyConnection(ip).login()
        device.writelock()

        if self.config_reset:
            print" Reset factory default : YES !!!"
            device.send("configuration reset force")

        if self.ip != 0:
            try:
                link_fw = "http://{ip}:{port}/_current_branch_/{platform}/latest.afl".format(ip=self.host_ip, port=HTTP_PORT, platform=self.platform)
                device.send_raw("firmware upgrade {}".format(link_fw))
                device.expect_raw(pexpect.EOF)
                print "[{}] Device is rebooting....".format(device.ip)
            except:
                print "Firmware upgrade: Unexpected error !"
                closeAllExpect()
                sys.exit(0)

    def build_and_upload(self):
        self.build_amef()
        self.upload_firmware()


    def start_http_server(self):
        print "******* Start SimpleHTTPServer on port {port} *********".format(port=HTTP_PORT)
    
    # Open shell for http server
    p_httpserver = pexpect.spawn("bash")
    
    # Store pexect session into list for manager:
    ls_expect.append(p_httpserver)
    
    p_httpserver.sendline("cd {path} ; python -m SimpleHTTPServer {port}".format(path=IMAGE_DIR, port=HTTP_PORT))
    rs = p_httpserver.expect("\.\.\.")


if __name__ == '__main__':

    signal.signal(signal.SIGINT, SIGINT_handler)

    parser = argparse.ArgumentParser(
                description="amefbuild: automatic build and upload firmware to device",
                formatter_class=argparse.HelpFormatter,
                )
    parser.add_argument('-i', action='store', dest='ip', help='Input device IP')
    parser.add_argument('-p', action='store', dest='platform', choices=['ETCHELL4', 'ETCHELL5', 'CLIPPER', 'VCX', 'FSX'] , help='Input device platform', required=True)
    parser.add_argument('-b', action='store_true', dest='action_build', default=False, help='Build firmware only, do not upload')
    parser.add_argument('-u', action='store_true', dest='action_upload', default=False, help='Upload lastest firmware to device')
    parser.add_argument('-bu', action='store_true', dest='action_double', default=False, help='Build and Upload lastest firmware to device')
    parser.add_argument('-r', action='store_true', dest='action_reset', default=False, help='[Optional] Reset factory default')
    
    args = parser.parse_args()

    device_ip=format(args.ip)
    device_platform = format(args.platform)
    is_reset = args.action_reset

    util_session = AmefBuild_Auto(ip=device_ip,platform=device_platform, config_reset=is_reset)

    if args.action_build:
        util_session.build_amef()
    elif args.action_upload:
        util_session.upload_firmware()
    elif args.action_double:
        util_session.build_and_upload()
    else:
        print "\n"
        print "No any actions ? Are you kidding me ?"
        print "Type '-h' for help"
        print "\n"

    closeAllExpect()